<?php

namespace Application\Migrations;

use BlogBundle\Entity\AsyncTaskType;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170115005826 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql('insert into async_task_type values(:id,:description)',
            ['id' => AsyncTaskType::DEPOSIT_TYPE, 'description' => 'task for deposit money']);

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('delete from async_task_type where id=:id',
            ['id' => AsyncTaskType::DEPOSIT_TYPE]);
    }
}
