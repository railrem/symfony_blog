<?php

namespace Application\Migrations;

use BlogBundle\Entity\Role;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170108003903 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql("insert into role values (:id,:role)",['id'=>Role::ROLE_USER,'role'=>'ROLE_USER']);
        $this->addSql("insert into role values (:id,:role)",['id'=>Role::ROLE_ADMIN,'role'=>'ROLE_ADMIN']);

        $this->addSql("ALTER TABLE public.user ADD COLUMN avatar_path character varying(255)");
        $this->addSql("ALTER TABLE post ADD COLUMN is_free boolean");
        $this->addSql('
        CREATE TABLE teg (
            id integer PRIMARY KEY,
            name   varchar(255) NOT NULL
            );
        ');
        $this->addSql('
        CREATE TABLE post_tegs (
post_id integer NOT NULL REFERENCES  post(id),
teg_id integer NOT NULL REFERENCES  teg(id)

            );
        ');


    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql("ALTER TABLE public.user DROP COLUMN avatar_path ");
        $this->addSql("ALTER TABLE post DROP COLUMN is_free ");
        $this->addSql('drop table teg');
        $this->addSql('drop table post_tegs');

    }
}
