<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170114231556 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql('create table transaction(
 id serial primary key,
 amount float,
target_user_id integer NOT NULL REFERENCES  public.user(id),
source_user_id integer NOT NULL REFERENCES  public.user(id),
datetime timestamp(0) without time zone NOT NULL,
is_payment boolean default false
)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('drop table `transaction`;');
    }
}
