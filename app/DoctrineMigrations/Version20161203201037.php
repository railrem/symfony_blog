<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161203201037 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
         //this up() migration is auto-generated, please modify it to your needs

        $this->addSql('
        CREATE TABLE country (
            id integer PRIMARY KEY,
            name   varchar(255) NOT NULL
            );
        ');
        $this->addSql('
        CREATE TABLE weather (
            id integer PRIMARY KEY,
            temp float,
            time timestamp(0) without time zone NOT NULL,
            country_id integer NOT NULL REFERENCES  country(id)
            );
        ');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('drop table weather');
        $this->addSql('drop table country');

        // this down() migration is auto-generated, please modify it to your needs

    }
}
