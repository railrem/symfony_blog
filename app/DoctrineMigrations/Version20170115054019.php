<?php

namespace Application\Migrations;

use BlogBundle\Entity\AsyncTaskType;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170115054019 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql('insert into async_task_type values(:id,:description)',
            ['id' => AsyncTaskType::BUY_POST_TYPE, 'description' => 'task for deposit money']);
        $this->addSql("CREATE TABLE user_post
(
  user_id integer NOT NULL,
  post_id integer NOT NULL,
  CONSTRAINT user_post_pkey PRIMARY KEY (user_id, post_id),
  CONSTRAINT fk_user_post_user FOREIGN KEY (user_id)
      REFERENCES public.user (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_user_post_post FOREIGN KEY (post_id)
      REFERENCES public.post (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)");

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('delete from async_task_type where id=:id',
            ['id' => AsyncTaskType::BUY_POST_TYPE]);
        $this->addSql("drop table user_post");
    }
}
