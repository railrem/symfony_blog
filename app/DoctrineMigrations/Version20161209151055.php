<?php

namespace Application\Migrations;

use BlogBundle\Entity\AsyncTaskType;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161209151055 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('create table async_task_type(
id integer primary key,
description varchar
)');
        $this->addSql('create table async_task(
 id serial primary key,
 content character varying(255),
type_id integer NOT NULL REFERENCES  async_task_type(id),
is_exe boolean default false
)');
        $this->addSql('insert into async_task_type values(:id,:description)',
            ['id' => AsyncTaskType::EMAIL_TYPE, 'description' => 'task for sending email']);
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('drop table async_task');
        $this->addSql('drop table async_task_type');
    }
}
