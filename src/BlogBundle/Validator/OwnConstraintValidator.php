<?php
namespace  BlogBundle\Validator;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Created by PhpStorm.
 * User: rail
 * Date: 09.12.16
 * Time: 17:42
 */

/**
 * @Annotation
 */
class OwnConstraintValidator extends ConstraintValidator
{

    /**
     * Checks if the passed value is valid.
     *
     * @param mixed $value The value that should be validated
     * @param Constraint $constraint The constraint for the validation
     */
    public function validate($value, Constraint $constraint)
    {
        $date = getdate();
        $hour =$date['hours'];
        if ($value!=$hour) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('%string%', $value)
                ->addViolation();
        }
    }
}