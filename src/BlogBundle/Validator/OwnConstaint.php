<?php
namespace BlogBundle\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class OwnConstraint extends Constraint
{
    public $message = 'Invalid Current hour';

    public function validatedBy()
    {
        return get_class($this).'Validator';
    }
}