<?php
/**
 * Created by PhpStorm.
 * User: rail
 * Date: 20.11.16
 * Time: 17:45
 */
namespace BlogBundle\Entity;

use BlogBundle\Entity\Post;
use BlogBundle\Entity\Profile;
use DateTime;
use Doctrine\ORM\Mapping as ORM;


/**
 * @param $post Post
 *
 * @ORM\Entity(repositoryClass="BlogBundle\Entity\Repository\LikeRepository")
 * @ORM\Table(name="public.like")
 */
class Like
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="User",cascade="persist",fetch="EAGER",inversedBy="likes")
     */
    protected $user;

    /**
     * @ORM\ManyToOne(targetEntity="Post",cascade={"persist"},fetch="EAGER",inversedBy="likes")
     */

    protected $post;

    /**
     * @ORM\Column(type="string")
     */
    protected $owner_ip;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $created_at;

    public function __construct()
    {
        $this->setCreatedAt(new DateTime());
    }

    /**
     * Get id
     *
     * @return \uuid
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ownerIp
     *
     * @param string $ownerIp
     *
     * @return Like
     */
    public function setOwnerIp($ownerIp)
    {
        $this->owner_ip = $ownerIp;

        return $this;
    }

    /**
     * Get ownerIp
     *
     * @return string
     */
    public function getOwnerIp()
    {
        return $this->owner_ip;
    }

    /**
     * Set createdAt
     *
     * @param DateTime $createdAt
     *
     * @return Like
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set user
     *
     * @param User $user
     *
     * @return Like
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set post
     *
     * @param Post $post
     *
     * @return Like
     */
    public function setPost(Post $post = null)
    {
        $this->post = $post;

        return $this;
    }

    /**
     * Get post
     *
     * @return Post
     */
    public function getPost()
    {
        return $this->post;
    }
}
