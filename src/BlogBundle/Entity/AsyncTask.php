<?php
/**
 * Created by PhpStorm.
 * User: rail
 * Date: 09.12.16
 * Time: 15:34
 */

namespace BlogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="BlogBundle\Entity\Repository\AsyncTaskRepository")
 * @ORM\Table(name="async_task")
 */
class AsyncTask
{

    public  function  __construct()
    {
        $this->setIsExe(false);
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     * @var string $content
     */
    protected $content;

    /**
     * @ORM\Column(type="boolean")
     * @var boolean $is_exe
     */
    protected $is_exe;


    /**
     * @ORM\ManyToOne(targetEntity="AsyncTaskType",cascade={"persist"},fetch="EAGER",inversedBy="asyncTasks")
     */
    protected $type;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return boolean
     */
    public function getIsExe()
    {
        return $this->is_exe;
    }

    /**
     * @param boolean $is_exe
     */
    public function setIsExe($is_exe)
    {
        $this->is_exe = $is_exe;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }


}