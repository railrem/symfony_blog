<?php
/**
 * Created by PhpStorm.
 * User: rail
 * Date: 07.12.16
 * Time: 3:14
 */

namespace BlogBundle\Entity\Repository;


use Doctrine\ORM\EntityRepository;

class TegRepository extends EntityRepository
{
    public function getAllIndexById(){
        $qb = $this->createQueryBuilder('t','t.id')
            ->select('t');
        return $qb->getQuery()
            ->getResult();
    }
}