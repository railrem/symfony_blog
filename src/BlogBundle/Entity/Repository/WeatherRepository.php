<?php
namespace BlogBundle\Entity\Repository;

use BlogBundle\Entity\Weather;
use Doctrine\ORM\EntityRepository;

class WeatherRepository extends EntityRepository
{
    public function save(Weather $weather)
    {
        $is_time_exist = $this->createQueryBuilder('w')
            ->where('w.time = :time')
            ->andWhere('w.country = :country')
            ->setParameters(['time' => $weather->getTime(),
                'country' => $weather->getCountry()])
            ->getQuery()
            ->getOneOrNullResult();
        if (is_null($is_time_exist)) {
            $this->getEntityManager()->persist($weather);
            $this->getEntityManager()->flush();
            return true;
        } else {
            return false;
        }

    }

    public function getCurrentForAllCityWeather()
    {
        $max_time = $this->createQueryBuilder('w')
            ->select('max(w.time)')
            ->getQuery()->getSingleScalarResult();
        $qb = $this->createQueryBuilder('w')
            ->select('w')
            ->where('w.time = :time')
            ->setParameter('time', $max_time);

        return $qb->getQuery()
            ->getResult();
    }
}