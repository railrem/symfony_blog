<?php
namespace BlogBundle\Entity\Repository;


use BlogBundle\Entity\AsyncTaskType;

class AsyncTaskTypeRepository extends \Doctrine\ORM\EntityRepository
{

    public function getEmailAsyncTaskType()
    {
        return $this->findOneBy(['id' => AsyncTaskType::EMAIL_TYPE]);
    }

    public function getDepositAsyncTaskType()
    {
        return $this->findOneBy(['id' => AsyncTaskType::DEPOSIT_TYPE]);
    }

    public function getBuyPostAsyncTaskType()
    {
        return $this->findOneBy(['id' => AsyncTaskType::BUY_POST_TYPE]);
    }
}