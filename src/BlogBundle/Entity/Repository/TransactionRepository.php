<?php
/**
 * Created by PhpStorm.
 * User: rail
 * Date: 09.12.16
 * Time: 15:28
 */

namespace BlogBundle\Entity\Repository;


use BlogBundle\Entity\Transaction;

class TransactionRepository extends \Doctrine\ORM\EntityRepository
{

    /**
     * @param Transaction $transaction
     */
    public function save(Transaction $transaction)
    {
        $this->getEntityManager()->persist($transaction);
        $this->getEntityManager()->flush();
    }

    public function getTransactionByUser($user)
    {
        return $this->createQueryBuilder('t')
            ->where('t.sourceUser = :user')
            ->orWhere("t.targetUser = :user")
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult();
    }

}