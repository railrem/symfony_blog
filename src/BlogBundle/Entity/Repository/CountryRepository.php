<?php
/**
 * Created by PhpStorm.
 * User: rail
 * Date: 07.12.16
 * Time: 3:14
 */

namespace BlogBundle\Entity\Repository;


use Doctrine\ORM\EntityRepository;

class CountryRepository extends EntityRepository
{
    public function getAllIndexById(){
        $qb = $this->createQueryBuilder('w','w.id')
            ->select('w');
        return $qb->getQuery()
            ->getResult();
    }
}