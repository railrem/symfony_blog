<?php
/**
 * Created by PhpStorm.
 * User: rail
 * Date: 09.12.16
 * Time: 15:28
 */

namespace BlogBundle\Entity\Repository;


use BlogBundle\Entity\AsyncTask;

class AsyncTaskRepository extends \Doctrine\ORM\EntityRepository
{

    public function saveAsyncTask(AsyncTask $asyncTask)
    {
        $this->getEntityManager()->persist($asyncTask);
        $this->getEntityManager()->flush();
    }

    public function saveEmailAsyncTask(AsyncTask $asyncTask)
    {
        $em = $this->getEntityManager();
        /** @var AsyncTaskTypeRepository $type_repository */
        $type_repository = $em->getRepository('BlogBundle:AsyncTaskType');
        $asyncTask->setType($type_repository->getEmailAsyncTaskType());
        $this->getEntityManager()->persist($asyncTask);
        $this->getEntityManager()->flush();
    }

    public function saveBuyPostAsyncTask(AsyncTask $asyncTask)
    {
        $em = $this->getEntityManager();
        /** @var AsyncTaskTypeRepository $type_repository */
        $type_repository = $em->getRepository('BlogBundle:AsyncTaskType');
        $asyncTask->setType($type_repository->getBuyPostAsyncTaskType());
        $this->getEntityManager()->persist($asyncTask);
        $this->getEntityManager()->flush();
    }

    public function getEmailTaskForExecute()
    {
        $em = $this->getEntityManager();
        /** @var AsyncTaskTypeRepository $type_repository */
        $type_repository = $em->getRepository('BlogBundle:AsyncTaskType');
        return $this->findBy(['is_exe' => false, 'type' => $type_repository->getEmailAsyncTaskType()]);
    }

    /**
     * @param AsyncTask $asyncTask
     */
    public function saveDepositAsyncTask(AsyncTask $asyncTask)
    {
        $em = $this->getEntityManager();
        /** @var AsyncTaskTypeRepository $type_repository */
        $type_repository = $em->getRepository('BlogBundle:AsyncTaskType');
        $asyncTask->setType($type_repository->getDepositAsyncTaskType());
        $this->getEntityManager()->persist($asyncTask);
        $this->getEntityManager()->flush();
    }

    /**
     * @return array
     */
    public function getDepositTaskForExecute()
    {
        $em = $this->getEntityManager();
        /** @var AsyncTaskTypeRepository $type_repository */
        $type_repository = $em->getRepository('BlogBundle:AsyncTaskType');
        return $this->findBy(['is_exe' => false, 'type' => $type_repository->getDepositAsyncTaskType()]);
    }

    /**
     * @return array
     */
    public function getTaskForExecute()
    {
        $em = $this->getEntityManager();
        /** @var AsyncTaskTypeRepository $type_repository */
        $type_repository = $em->getRepository('BlogBundle:AsyncTaskType');
        return $this->findBy(['is_exe' => false]);
    }
}