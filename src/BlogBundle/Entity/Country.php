<?php
namespace BlogBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="BlogBundle\Entity\Repository\CountryRepository")
 * @ORM\Table(name="country")
 */
class Country
{

    public function __construct()
    {
        $this->weathers = new  ArrayCollection();
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     */
    protected $id;


    /**
     * @ORM\Column(type="string",nullable=false)
     * @var string $name
     */
    protected $name;

    /**
     * @ORM\OneToMany(targetEntity="Weather", mappedBy="country")
     */
    protected $weathers;

    /**
     * @return mixed
     */
    public function getWeathers()
    {
        return $this->weathers;
    }

    /**
     * @param mixed $weathers
     */
    public function setWeathers($weathers)
    {
        $this->weathers = $weathers;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

}