<?php
namespace BlogBundle\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="BlogBundle\Entity\Repository\PostRepository")
 * @ORM\Table(name="post")
 */
class Post
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     */
    protected $id;

    /**
     * @ORM\Column(type="string",nullable=false)
     */
    protected $title;

    /**
     * @ORM\Column(type="string",nullable=false)
     */
    protected $body;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $is_free = true;

    /**
     * @ORM\ManyToOne(targetEntity="User",cascade="persist",fetch="EAGER",inversedBy="posts")
     */
    protected $user;


    /**
     * One Product has Many Features.
     * @ORM\OneToMany(targetEntity="Like", mappedBy="post",fetch="EAGER")
     */
    protected $likes;


    /**
     * @ORM\ManyToMany(targetEntity="Teg")
     * @ORM\JoinTable(name="post_tegs",
     *     joinColumns={@ORM\JoinColumn(name="post_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="teg_id", referencedColumnName="id")}
     * )
     *
     * @var ArrayCollection $posts
     */
    protected $tegs;


    /**
     * @return Like[]
     */
    public function getLikes()
    {
        return $this->likes;
    }

    /**
     * @param ArrayCollection $likes
     */
    public function setLikes($likes)
    {
        $this->likes = $likes;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @ORM\Column(type="datetime")
     */
    protected $created_at;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $updated_at;

    public function __construct()
    {
        $this->setLikes(new ArrayCollection());
        $this->setTegs(new ArrayCollection());
        $this->setCreatedAt(new DateTime());
        $this->setUpdatedAt(new DateTime());
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Post
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }


    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set body
     *
     * @param string $body
     *
     * @return Post
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Post
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Post
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set profile
     *
     * @param User $user
     *
     * @return Post
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get profile
     *
     * @return User
     */
    public function getProfile()
    {
        return $this->user;
    }

    /**
     * @return ArrayCollection
     */
    public function getTegs()
    {
        return $this->tegs;
    }

    /**
     * @param ArrayCollection $tegs
     */
    public function setTegs($tegs)
    {
        $this->tegs = $tegs;
    }

    /**
     * @return mixed
     */
    public function getIsFree()
    {
        return $this->is_free;
    }

    /**
     * @param mixed $is_free
     */
    public function setIsFree($is_free)
    {
        $this->is_free = $is_free;
    }


}
