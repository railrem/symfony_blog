<?php
namespace BlogBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Serializable;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\VarDumper\VarDumper;

/**
 * @ORM\Entity(repositoryClass="BlogBundle\Entity\Repository\UserRepository")
 * @ORM\Table(name="public.user")
 * @UniqueEntity(fields="email", message="Sorry, this email address is already in use.")
 * @UniqueEntity(fields="username", message="Sorry, this username is already taken.")
 */
class User implements UserInterface, Serializable
{


    /**
     * Конструктор класса User
     */
    public function __construct()
    {
        $this->posts = new  ArrayCollection();
        $this->inTransactions = new ArrayCollection();
        $this->outTransactions = new ArrayCollection();
        $this->likes = new ArrayCollection();
        $this->userRoles = new ArrayCollection();
        $this->userAssignPosts = new ArrayCollection();
        $this->created_at = new \DateTime();
        $this->balance = 0;
    }


    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     */
    protected $id;

    /**
     * @ORM\Column(type="string",unique=true,nullable=false)
     * @Assert\NotBlank()
     */
    protected $username;

    /**
     * @ORM\Column(type="string",unique=true,nullable=false)
     * @Assert\NotBlank()
     */
    protected $avatar_path;

    /**
     * @ORM\Column(type="string",nullable=false)
     */
    protected $password;


    /**
     * @ORM\ManyToMany(targetEntity="Role")
     * @ORM\JoinTable(name="user_role",
     *     joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id")}
     * )
     *
     * @var ArrayCollection $userRoles
     */
    protected $userRoles;


    /**
     * @ORM\ManyToMany(targetEntity="Post")
     * @ORM\JoinTable(name="user_post",
     *     joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="post_id", referencedColumnName="id")}
     * )
     *
     * @var ArrayCollection $userAssignPosts
     */
    protected $userAssignPosts;

    /**
     * @ORM\Column(type="string", unique=true, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    protected $email;


    /**
     * @ORM\Column(type="datetime")
     */
    protected $created_at;

    /**
     * @ORM\Column(type="float")
     */
    protected $balance;

    /**
     *
     * @Assert\NotBlank()
     * @Assert\Length(max=4096)
     */
    protected $password_repeat;

    /**
     * @return mixed
     */
    public function getUserAssignPosts()
    {
        return $this->userAssignPosts;
    }

    /**
     * @param mixed $userAssignPosts
     */
    public function setUserAssignPosts($userAssignPosts)
    {
        $this->userAssignPosts = $userAssignPosts;
    }


    /**
     * @return mixed
     */
    public function getAvatarPath()
    {
        return $this->avatar_path;
    }

    /**
     * @param mixed $avatar_path
     */
    public function setAvatarPath($avatar_path)
    {
        $this->avatar_path = $avatar_path;
    }


    /**
     * @return mixed
     */
    public function getPasswordRepeat()
    {
        return $this->password_repeat;
    }

    /**
     * @param mixed $password_repeat
     */
    public function setPasswordRepeat($password_repeat)
    {
        $this->password_repeat = $password_repeat;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }


    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return User
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPosts()
    {
        return $this->posts;
    }

    /**
     * @param mixed $posts
     */
    public function setPosts($posts)
    {
        $this->posts = $posts;
    }

    /**
     * @return mixed
     */
    public function getLikes()
    {
        return $this->likes;
    }

    /**
     * @param mixed $likes
     */
    public function setLikes($likes)
    {
        $this->likes = $likes;
    }

    /**
     * @ORM\OneToMany(targetEntity="Post", mappedBy="user")
     */
    protected $posts;

    /**
     * @ORM\OneToMany(targetEntity="Transaction", mappedBy="targetUser")
     */
    protected $inTransactions;


    /**
     * @ORM\OneToMany(targetEntity="Transaction", mappedBy="sourceUser")
     */
    protected $outTransactions;

    /**
     * @ORM\OneToMany(targetEntity="Like", mappedBy="user")
     */
    protected $likes;

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Returns the roles granted to the user.
     *
     * <code>
     * public function getRoles()
     * {
     *     return array('ROLE_USER');
     * }
     * </code>
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return (Role|string)[] The user roles
     */
    public function getRoles()
    {
        return $this->getUserRoles()->toArray();
    }

    public function setRole($role)
    {
        $this->userRoles[] = $role;
    }

    /**
     * @return mixed
     */
    public function getInTransactions()
    {
        return $this->inTransactions;
    }

    /**
     * @param mixed $inTransactions
     */
    public function setInTransactions($inTransactions)
    {
        $this->inTransactions = $inTransactions;
    }

    /**
     * @return mixed
     */
    public function getOutTransactions()
    {
        return $this->outTransactions;
    }

    /**
     * @param mixed $outTransactions
     */
    public function setOutTransactions($outTransactions)
    {
        $this->outTransactions = $outTransactions;
    }


    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
        $this->password = "";
    }

    /**
     * Геттер для ролей пользователя.
     *
     * @return ArrayCollection A Doctrine ArrayCollection
     */
    public function getUserRoles()
    {
        return $this->userRoles;
    }

    /**
     * @return mixed
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * @param mixed $balance
     */
    public function setBalance($balance)
    {
        $this->balance = $balance;
    }


    /**
     * String representation of object
     * @link http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     * @since 5.1.0
     */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->email,
            $this->password,
        ));
    }

    /**
     * Constructs the object
     * @link http://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     * @since 5.1.0
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->email,
            $this->password,
            ) = unserialize($serialized);
    }

    /**
     * @return bool
     */
    public function saveAvatar()
    {
        $web_dir = '/uploads/avatars/' . self::randStrGen(10);
        $server_new_path = '/var/www/study_symfony/web' . $web_dir;

        if (move_uploaded_file($this->getAvatarPath()->getRealPath(), $server_new_path)) {
            $this->avatar_path = $web_dir;
            return true;
        };
        return false;
    }

    /**
     * @param $len
     * @return string
     */
    static function randStrGen($len)
    {
        $result = "";
        $chars = "abcdefghijklmnopqrstuvwxyz0123456789";
        $charArray = str_split($chars);
        for ($i = 0; $i < $len; $i++) {
            $randItem = array_rand($charArray);
            $result .= "" . $charArray[$randItem];
        }
        return $result;
    }

    public function addAssigmentForPost($post)
    {
        /** @var ArrayCollection $posts */
        $posts = $this->getUserAssignPosts();
        $posts->add($post);
        $this->setUserAssignPosts($posts);
    }
}
