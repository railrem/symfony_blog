<?php
/**
 * Created by PhpStorm.
 * User: rail
 * Date: 09.12.16
 * Time: 15:34
 */

namespace BlogBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Constraints as Assert;
use DateTime as DT;
/**
 * @ORM\Entity(repositoryClass="BlogBundle\Entity\Repository\TransactionRepository")
 * @ORM\Table(name="transaction")
 */
class Transaction
{


    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     */
    protected $id;


    /**
     * @ORM\Column(type="float")
     * @Assert\NotNull()
     * @Assert\GreaterThan(
     *     value = 0
     * )
     *  * @Assert\LessThanOrEqual(
     *     value = 1000
     * )
     */
    protected $amount;


    /**
     * @ORM\ManyToOne(targetEntity="User",cascade="persist",fetch="EAGER",inversedBy="outTransactions")
     */
    protected $sourceUser;

    /**
     * @ORM\ManyToOne(targetEntity="User",cascade="persist",fetch="EAGER",inversedBy="inTransactions")
     */
    protected $targetUser;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $is_payment;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $datetime;

    /**
     * Transaction constructor.
     * @param $id
     * @param $amount
     * @param $sourceUser
     * @param $targetUser
     * @param $is_payment
     */
    public function __construct()
    {
        $this->sourceUser = new ArrayCollection();
        $this->targetUser = new ArrayCollection();
        $this->is_payment = false;
        $this->setDatetime(new DT());
    }

    /**
     * @return mixed
     */
    public function getDatetime()
    {
        return $this->datetime;
    }

    /**
     * @param mixed $datetime
     */
    public function setDatetime($datetime)
    {
        $this->datetime = $datetime;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getSourceUser()
    {
        return $this->sourceUser;
    }

    /**
     * @param mixed $sourceUser
     */
    public function setSourceUser($sourceUser)
    {
        $this->sourceUser = $sourceUser;
    }

    /**
     * @return mixed
     */
    public function getTargetUser()
    {
        return $this->targetUser;
    }

    /**
     * @param mixed $targetUser
     */
    public function setTargetUser($targetUser)
    {
        $this->targetUser = $targetUser;
    }

    /**
     * @return mixed
     */
    public function getIsPayment()
    {
        return $this->is_payment;
    }

    /**
     * @param mixed $is_payment
     */
    public function setIsPayment($is_payment)
    {
        $this->is_payment = $is_payment;
    }


}