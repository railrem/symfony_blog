<?php
namespace BlogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="BlogBundle\Entity\Repository\AsyncTaskTypeRepository")
 * @ORM\Table(name="async_task_type")
 */
class AsyncTaskType
{
    const EMAIL_TYPE = 10;
    const DEPOSIT_TYPE = 20;
    const BUY_POST_TYPE = 30;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     *
     * @var integer $id
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     * @var string $description
     */
    protected $description;

    /**
     * @ORM\OneToMany(targetEntity="AsyncTask", mappedBy="async_task_type")
     */
    protected $asyncTasks;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getAsyncTasks()
    {
        return $this->asyncTasks;
    }

    /**
     * @param mixed $asyncTasks
     */
    public function setAsyncTasks($asyncTasks)
    {
        $this->asyncTasks = $asyncTasks;
    }


}