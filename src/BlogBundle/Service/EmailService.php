<?php
/**
 * Created by PhpStorm.
 * User: rail
 * Date: 30.11.16
 * Time: 19:29
 */

namespace BlogBundle\Service;


use Swift_Mailer;
use Swift_Message;
use Symfony\Component\VarDumper\VarDumper;

class EmailService
{

    /** @var  Swift_Message */
    protected  $message;
    protected $contact_email;
    /** @var  Swift_Mailer */
    protected $mailer;

    public function _construct($contact_email,Swift_Mailer $mailer){
        $this->message = Swift_Message::newInstance();
        $this->contact_email = $contact_email;
        $this->mailer = $mailer;
    }

    public function sendMessage($email,$subject,$content){
        $message = Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom('kfubloggers@gmail.com')
            ->setTo($email)
            ->setBody($content);

        $this->mailer->send($message);
    }

}