<?php
/**
 * Created by PhpStorm.
 * User: rail
 * Date: 30.11.16
 * Time: 16:14
 */

namespace BlogBundle\Service;

use BlogBundle\Entity\Repository\RoleRepository;
use BlogBundle\Entity\Repository\UserRepository;
use BlogBundle\Entity\Role;
use BlogBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;
use Symfony\Component\VarDumper\VarDumper;

class SecurityService
{
    /**
 *
 * @var EntityManager
 */
    private $em;

    /**
     * @var MessageDigestPasswordEncoder
     */
    private $encoder;


    public function __construct(EntityManager $em,$encoder){
        $this->em = $em;
        $this->encoder = $encoder;
    }

    public function register(User $user,$role = null){

        $user->setPassword(
            $this->encoder->encodePassword($user, $user->getPasswordRepeat())
        );

        if (is_null($role)){
            /** @var RoleRepository $role_repository */
            $role_repository = $this->em->getRepository('BlogBundle:Role');
            $role = $role_repository->getRoleById(Role::ROLE_USER);
        }

        $user->getUserRoles()->add($role);
        /** @var UserRepository $user_repository */
        $user_repository = $this->em->getRepository('BlogBundle:User');
        $user_repository->save($user);

    }
}