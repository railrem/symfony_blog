<?php
/**
 * Created by PhpStorm.
 * User: rail
 * Date: 25.11.16
 * Time: 10:46
 */

namespace BlogBundle\Form;

use Doctrine\ORM\Mapping\UniqueConstraint;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Mapping\ClassMetadata;

class SignupForm extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class)
            ->add('username', TextType::class)
            ->add('password_repeat', RepeatedType::class, array(
                    'type' => PasswordType::class,
                    'first_options' => array('label' => 'Password'),
                    'second_options' => array('label' => 'Repeat Password'),
                )
            )
            ->add('avatar_path', FileType::class);
    }

    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraints('username', [new NotBlank(), new UniqueConstraint()]);
        $metadata->addPropertyConstraints('password_repeat', [new NotBlank(), new Length(['min' => 6])]);
        $metadata->addPropertyConstraints('email', [new NotBlank(), new Email(), new UniqueConstraint()]);
        $metadata->addPropertyConstraints('avatar_path', [new NotBlank(), new File(['maxSize' => '1M', 'mimeTypes' => ['image/jpeg', 'image/png', 'image/pjpeg', 'image/gif']])]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BlogBundle\Entity\User',
        ));
    }

}