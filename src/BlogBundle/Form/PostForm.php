<?php
/**
 * Created by PhpStorm.
 * User: rail
 * Date: 30.11.16
 * Time: 21:42
 */

namespace BlogBundle\Form;


use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Mapping\ClassMetadata;

class PostForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('is_free', CheckboxType::class, array('label' => "Бесплатная запись","required"=>false));
        $builder->add('title', TextType::class,array('label' => "Тема"));
        $builder->add('body', CKEditorType::class, array('label' => false));
    }


    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraints('title', [new Length([
            'max' => 255]), new NotBlank()]);
        $metadata->addPropertyConstraints('body', [new Length([
            'max' => 255]), new NotBlank()]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }

    public function getBlockPrefix()
    {
        return 'post';
    }
}