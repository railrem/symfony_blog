<?php
/**
 * Created by PhpStorm.
 * User: rail
 * Date: 09.12.16
 * Time: 16:40
 */

namespace BlogBundle\Command;


use BlogBundle\Entity\AsyncTask;
use BlogBundle\Entity\Repository\AsyncTaskRepository;
use BlogBundle\Entity\Repository\UserRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\VarDumper\VarDumper;

class EmailCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('email:queue')
            ->setDescription('Sent email address');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var EntityManager $entityManager */
        $entityManager = $this->getContainer()->get('doctrine')->getEntityManager();
        /** @var AsyncTaskRepository $asyncTaskRepository */
        $asyncTaskRepository = $entityManager->getRepository('BlogBundle:AsyncTask');
        /** @var UserRepository $user_repository */
        $user_repository = $entityManager->getRepository('BlogBundle:User');

        /** @var AsyncTask $task */
        foreach ($asyncTaskRepository->getEmailTaskForExecute() as $task) {
            foreach ($user_repository->getAllUserEmails() as $email) {
                $email = $email['email'];
                $message = \Swift_Message::newInstance()
                    ->setSubject("Contact request")
                    ->setFrom($this->getContainer()->getParameter('contact_email'))
                    ->setTo($email)
                    ->setBody($task->getContent());
                $this->getContainer()->get('mailer')->send($message);
            }
            $task->setIsExe(true);
            $asyncTaskRepository->saveAsyncTask($task);
        }

        $output->writeln('DONE');


    }
}