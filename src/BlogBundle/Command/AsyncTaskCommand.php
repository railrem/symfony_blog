<?php
/**
 * Created by PhpStorm.
 * User: rail
 * Date: 09.12.16
 * Time: 16:40
 */

namespace BlogBundle\Command;


use BlogBundle\Entity\AsyncTask;
use BlogBundle\Entity\AsyncTaskType;
use BlogBundle\Entity\Post;
use BlogBundle\Entity\Repository\AsyncTaskRepository;
use BlogBundle\Entity\Repository\PostRepository;
use BlogBundle\Entity\Repository\UserRepository;
use BlogBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\VarDumper\VarDumper;

class AsyncTaskCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('asyncWorker:start')
            ->setDescription('exe async task');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var EntityManager $entityManager */
        $entityManager = $this->getContainer()->get('doctrine')->getEntityManager();
        /** @var AsyncTaskRepository $asyncTaskRepository */
        $asyncTaskRepository = $entityManager->getRepository('BlogBundle:AsyncTask');

        while (true) {
            /** @var AsyncTask $task */
            foreach ($asyncTaskRepository->getTaskForExecute() as $task) {
                /** @var AsyncTaskType $task_type */
                $task_type = $task->getType();
                if ($task_type->getId() == AsyncTaskType::DEPOSIT_TYPE) {
                    $this->exeDepositTask($task, $asyncTaskRepository, $input, $output);
                }
                if ($task_type->getId() == AsyncTaskType::EMAIL_TYPE) {
                    $this->exeEmailTask($task, $asyncTaskRepository, $input, $output);
                }
                if ($task_type->getId() == AsyncTaskType::BUY_POST_TYPE) {
                    $this->exeBuyPostTask($task, $asyncTaskRepository, $input, $output);
                }
            }
        }
    }

    /**
     * @param $task AsyncTask
     * @param $asyncTaskRepository AsyncTaskRepository
     */
    public function exeEmailTask($task, $asyncTaskRepository, InputInterface $input, OutputInterface $output)
    {
        /** @var EntityManager $entityManager */
        $entityManager = $this->getContainer()->get('doctrine')->getEntityManager();
        /** @var UserRepository $user_repository */
        $user_repository = $entityManager->getRepository('BlogBundle:User');
        foreach ($user_repository->getAllUserEmails() as $email) {
            $email = $email['email'];
            $message = \Swift_Message::newInstance()
                ->setSubject("Contact request")
                ->setFrom($this->getContainer()->getParameter('contact_email'))
                ->setTo($email)
                ->setBody($task->getContent());
            $this->getContainer()->get('mailer')->send($message);
        }
        $task->setIsExe(true);
        $asyncTaskRepository->saveAsyncTask($task);
        $output->writeln("Send email to {$email}");

    }

    /**
     * @param $task AsyncTask
     * @param $asyncTaskRepository AsyncTaskRepository
     */
    public function exeDepositTask($task, $asyncTaskRepository, InputInterface $input, OutputInterface $output)
    {
        /** @var EntityManager $entityManager */
        $entityManager = $this->getContainer()->get('doctrine')->getEntityManager();
        /** @var UserRepository $user_repository */
        $user_repository = $entityManager->getRepository('BlogBundle:User');

        /** @var AsyncTask $task */
        $data = json_decode($task->getContent(), true);
        $user_id = (int)$data['user_id'];
        /** @var User $user */
        $user = $user_repository->find($user_id);

        $balance = $user->getBalance() + (float)$data['amount'];
        $user->setBalance($balance);
        $user_repository->save($user);
        $task->setIsExe(true);
        $output->writeln("Deposit {$data['amount']} to user with id {$user_id}");
        $asyncTaskRepository->saveAsyncTask($task);
    }

    /**
     * @param $task AsyncTask
     * @param $asyncTaskRepository AsyncTaskRepository
     */
    public function exeBuyPostTask($task, $asyncTaskRepository, InputInterface $input, OutputInterface $output)
    {
        /** @var EntityManager $entityManager */
        $entityManager = $this->getContainer()->get('doctrine')->getEntityManager();
        /** @var UserRepository $user_repository */
        $user_repository = $entityManager->getRepository('BlogBundle:User');
        /** @var PostRepository $post_repository */
        $post_repository = $entityManager->getRepository('BlogBundle:Post');
        /** @var AsyncTask $task */
        $data = json_decode($task->getContent(), true);
        $user_id = (int)$data['source_user_id'];
        $post_id = (int)$data['post_id'];

        /** @var User $sorce_user */
        $sorce_user = $user_repository->find($user_id);
        /** @var Post $post */
        $post = $post_repository->find($post_id);
        /** @var User $target_user */
        $target_user = $post->getUser();
        $price = 100;
        $percent = 50;
        $profit = $target_user->getBalance() + ($price / 100) * $percent;
        $target_user->setBalance($profit);
        $less = $sorce_user->getBalance() - $price;
        $sorce_user->setBalance($less);
        $sorce_user->addAssigmentForPost($post);
        $user_repository->save($sorce_user);
        $user_repository->save($target_user);
        $task->setIsExe(true);
        $output->writeln("User {$sorce_user->getUsername()} buy post {$post_id}");
        $asyncTaskRepository->saveAsyncTask($task);
    }

}