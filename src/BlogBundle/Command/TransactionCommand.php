<?php
/**
 * Created by PhpStorm.
 * User: rail
 * Date: 15.01.17
 * Time: 4:18
 */

namespace BlogBundle\Command;


use BlogBundle\Entity\AsyncTask;
use BlogBundle\Entity\Repository\AsyncTaskRepository;
use BlogBundle\Entity\Repository\UserRepository;
use BlogBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\VarDumper\VarDumper;

class TransactionCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('transaction:deposits')
            ->setDescription('Transfers money to user');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        while (true) {
            /** @var EntityManager $entityManager */
            $entityManager = $this->getContainer()->get('doctrine')->getEntityManager();
            /** @var AsyncTaskRepository $asyncTaskRepository */
            $asyncTaskRepository = $entityManager->getRepository('BlogBundle:AsyncTask');
            /** @var UserRepository $user_repository */
            $user_repository = $entityManager->getRepository('BlogBundle:User');

            /** @var AsyncTask $task */
            foreach ($asyncTaskRepository->getDepositTaskForExecute() as $task) {
                $data = json_decode($task->getContent(), true);
                $user_id = (int)$data['user_id'];
                /** @var User $user */
                $user = $user_repository->find($user_id);

                $balance = $user->getBalance() + (float)$data['amount'];
                $user->setBalance($balance);
                $user_repository->save($user);
                $task->setIsExe(true);
                $output->writeln("Deposit {$data['amount']} to user with id {$user_id}");
                $asyncTaskRepository->saveAsyncTask($task);
            }
        }
    }

}