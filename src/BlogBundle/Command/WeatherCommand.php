<?php
namespace BlogBundle\Command;

use BlogBundle\Entity\Country;
use BlogBundle\Entity\Repository\CountriesRepository;
use BlogBundle\Entity\Repository\CountryRepository;
use BlogBundle\Entity\Repository\WeatherRepository;
use BlogBundle\Entity\Weather;
use Buzz\Browser;
use Buzz\Message\Response;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\VarDumper\VarDumper;
use Symfony\Component\Validator\Constraints\DateTime;

class WeatherCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('weather:know')
            ->setDescription('Write to database actual weather value in another service');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var EntityManager $entityManager */
        $entityManager = $this->getContainer()->get('doctrine')->getEntityManager();
        /** @var WeatherRepository $weather_repository */
        $weather_repository = $entityManager->getRepository('BlogBundle:Weather');
        /** @var CountryRepository $country_repository */
        $country_repository = $entityManager->getRepository('BlogBundle:Country');
        $weather_api = $this->getContainer()->getParameter('weather_api');
        $weather_access_token = $this->getContainer()->getParameter('weather_access_token');
        $city_ids = '';
        $countries = $country_repository->getAllIndexById();
        /** @var Country $country */
        foreach ($countries as $id => $country) {
            $city_ids .= $id . ',';
        }
        $city_ids = substr($city_ids, 0, strlen($city_ids) - 1);
        $parameters = ['id' => $city_ids, 'APPID' => $weather_access_token];
        $api_url = $this->buildUrl($weather_api, $parameters);
        $datetime = new \DateTime();
        /** @var Browser $browser */
        $browser = $this->getContainer()->get('buzz.browser');
        /** @var Response $response */
        $response = $browser->get($api_url);
        if ($response->isOk()) {
            $result = json_decode($response->getContent(), true);
            foreach ($result['list'] as $item) {
                $id = $item['id'];
                $country = $countries[$id];
                $weather = new Weather();
                $weather->setTime($datetime);
                $weather->setCountry($country);
                $temp = (double)$item['main']['temp'] - 273.15;
                $weather->setTemp($temp);
                if (!$weather_repository->save($weather)) {
                    $output->writeln('weather for this town is actual!');
                }
            }
            $output->writeln('DONE');
        }

    }

    public function buildUrl($url, $parameters)
    {
        $url = $url . '?';
        foreach ($parameters as $key => $value) {
            $url = $url . $key . '=' . $value . '&';
        }
        return substr($url, 0, strlen($url) - 1);
    }
}