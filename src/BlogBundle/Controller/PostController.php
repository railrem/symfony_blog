<?php
/**
 * Created by PhpStorm.
 * User: rail
 * Date: 30.11.16
 * Time: 21:36
 */

namespace BlogBundle\Controller;


use BlogBundle\Entity\AsyncTask;
use BlogBundle\Entity\Like;
use BlogBundle\Entity\Post;
use BlogBundle\Entity\Repository\AsyncTaskRepository;
use BlogBundle\Entity\Repository\LikeRepository;
use BlogBundle\Entity\Repository\PostRepository;
use BlogBundle\Entity\Repository\UserRepository;
use BlogBundle\Entity\User;
use BlogBundle\Form\PostForm;
use BlogBundle\Helpers\VisitorHelper;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\VarDumper\VarDumper;


class PostController extends Controller
{


    /**
     * @Route("/post/view/{post_id}", name="post")
     */
    public function postAction($post_id)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var PostRepository $post_repository */
        $post_repository = $em->getRepository('BlogBundle:Post');
        /** @var LikeRepository $like_repository */
        $like_repository = $em->getRepository('BlogBundle:Like');
        /** @var Post $post */
        $post = $post_repository->find($post_id);
        /** @var User $user */
        $user = $this->getUser();
        $assign = false;
        if (!$post->getIsFree()) {
            /** @var Post $assign_post */
            foreach ($user->getUserAssignPosts() as $assign_post) {
                if ($assign_post->getId() == $post->getId()) {
                    $assign = true;
                }
            }
        } else {
            $assign = true;
        }
        if ($user) {
            $likes = $like_repository->getLikesByUser($user);
        } else {
            $owner_ip = VisitorHelper::getRealIpAddr();
            $likes = $like_repository->getLikesByIp($owner_ip);
        }
        return $this->render('BlogBundle:Post:post.html.twig',
            ['likes' => $likes, 'post' => $post, 'assign' => $assign]
        );
    }

    /**
     * @Route("/post/list/{user_id}", name="user_post_list")
     */
    public function postslistAction($user_id)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var PostRepository $post_repository */
        $post_repository = $em->getRepository('BlogBundle:Post');
        /** @var LikeRepository $like_repository */
        $user_repository = $em->getRepository('BlogBundle:User');
        $posts_user = $user_repository->find($user_id);
        $posts = $post_repository->getPostByUser($posts_user);
        return $this->render('BlogBundle:Post:posts_list.html.twig', ['posts' => $posts]);
    }

    /**
     * @Route("/post/user/{user_id}", name="user_posts")
     */
    public function postsAction($user_id)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var PostRepository $post_repository */
        $post_repository = $em->getRepository('BlogBundle:Post');
        /** @var LikeRepository $like_repository */
        $like_repository = $em->getRepository('BlogBundle:Like');
        /** @var UserRepository $user_repository */
        $user_repository = $em->getRepository('BlogBundle:User');
        $posts_user = $user_repository->find($user_id);
        $posts = $post_repository->getPostByUser($posts_user);
        $user = $this->getUser();
        if ($user) {
            $likes = $like_repository->getLikesByUser($user);
        } else {
            $owner_ip = VisitorHelper::getRealIpAddr();
            $likes = $like_repository->getLikesByIp($owner_ip);
        }
        return $this->render('BlogBundle:Post:posts.html.twig', ['likes' => $likes, 'posts' => $posts]);
    }

    /**
     * @Route("/post/add", name="post_add")
     */
    public function addAction(Request $request)
    {
        $post = new Post();
        $form = $this->createForm(PostForm::class, $post);
        $form->handleRequest($request);
        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            /** @var User $user */
            $user = $this->getUser();
            $post->setUser($user);
            /** @var PostRepository $post_repository */
            $post_repository = $em->getRepository('BlogBundle:Post');
            $post_repository->save($post);
            /** @var AsyncTaskRepository $async_task_repository */
            $async_task_repository = $em->getRepository('BlogBundle:AsyncTask');
            $async_task = new AsyncTask();
            $async_task->setContent($user->getUsername() . ' опубликовал новый пост!))');
            $async_task_repository->saveEmailAsyncTask($async_task);
            return $this->redirect($this->generateUrl('homepage'));
        }
        return $this->render('BlogBundle:Post:create.html.twig', ['form' => $form->createView()]);

    }

    /**
     * @Route("/post/like", name="post_like")
     */
    public function likeAction(Request $request)
    {
        $post_id = $request->get('post_id');
        $em = $this->getDoctrine()->getEntityManager();
        /** @var LikeRepository $like_repository */
        $like_repository = $em->getRepository('BlogBundle:Like');
        /** @var PostRepository $post_repository */
        $post_repository = $em->getRepository('BlogBundle:Post');
        $post = $post_repository->find($post_id);
        /** @var User $user */
        $user = $this->getUser();
        $like = new Like();
        $like->setPost($post);
        if (!is_null($user)) {
            if (!is_null($like_repository->getLikeByUserOrPost($user, $post))) {
                return new JsonResponse(['status' => 'error', 'message' => 'Лайк уже поставлен']);
            } else {
                $like->setUser($user);
            }
        } else {
            $owner_ip = VisitorHelper::getRealIpAddr();
            if (!is_null($like_repository->getLikeByIpOrPost($owner_ip, $post))) {
                return new JsonResponse(['status' => 'error', 'message' => 'Лайк уже поставлен']);

            } else {
                $like->setOwnerIp($owner_ip);
            }
        }
        $like_repository->save($like);
        return new JsonResponse(['status' => 'success']);
    }

    /**
     * @Route("/post/unlike", name="post_unlike")
     */
    public function unlikeAction(Request $request)
    {
        $post_id = $request->get('post_id');
        $em = $this->getDoctrine()->getEntityManager();
        /** @var LikeRepository $like_repository */
        $like_repository = $em->getRepository('BlogBundle:Like');
        /** @var User $user */
        $user = $this->getUser();
        if (!is_null($user)) {
            /** @var Like $like */
            $like = $like_repository->getLikeByUserOrPost($user->getId(), $post_id);
            if (is_null($like)) {
                return new JsonResponse(['status' => 'error', 'message' => 'Лайк не существует']);
            } else {
                $like_repository->delete($like->getId());
            }
        } else {
            $owner_ip = VisitorHelper::getRealIpAddr();
            $like = $like_repository->getLikeByIpOrPost($owner_ip, $post_id);
            if (is_null($like)) {
                return new JsonResponse(['status' => 'error', 'message' => 'Лайк не существует']);
            } else {
                $like_repository->delete($like->getId());
            }
        }
        return new JsonResponse(['status' => 'success']);
    }
}