<?php
namespace BlogBundle\Controller;

use BlogBundle\Entity\Contact;
use BlogBundle\Entity\Post;
use BlogBundle\Entity\Repository\LikeRepository;
use BlogBundle\Entity\Repository\PostRepository;
use BlogBundle\Entity\Repository\UserRepository;
use BlogBundle\Entity\Repository\WeatherRepository;
use BlogBundle\Entity\User;
use BlogBundle\Form\ContactForm;
use BlogBundle\Form\PostForm;
use BlogBundle\Helpers\VisitorHelper;
use BlogBundle\Service\EmailService;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\VarDumper\VarDumper;


class PageController extends Controller
{

    /**
     * @Route("/", name="default")
     */
    public function defaultAction()
    {
        return $this->redirect($this->generateUrl('homepage'));
    }

    /**
     * @Route("/home/{page}", name="homepage")
     */
    public function indexAction($page = 1)
    {
        $pagination = 4;
        $em = $this->getDoctrine()->getManager();
        /** @var PostRepository $post_repository */
        $post_repository = $em->getRepository('BlogBundle:Post');
        /** @var LikeRepository $like_repository */
        $like_repository = $em->getRepository('BlogBundle:Like');
        $posts = $post_repository->getAll();
        $count = count($posts);
        if ($count != 0) {
            $paginator_count = 1;
            if ($count > $pagination) {
                $paginator_count = $count / $pagination;
            }
            if (floor($paginator_count) != (float)$paginator_count) {
                $paginator_count = floor($paginator_count) + 1;
            }
            $finish_index = $page * $pagination - 1;
            $start_index = $pagination * ($page - 1);
            if ($start_index != 0) {
                $start_index = $start_index - 1;
            }

            if ($count - 1 <= $finish_index) {
                $posts = array_slice($posts, $start_index, $pagination);
            } else {
                $posts = array_slice($posts, $start_index, $finish_index + 1);
            }
        } else {
            $paginator_count = 0;
        }
        $user = $this->getUser();
        if ($user) {
            $likes = $like_repository->getLikesByUser($user);
        } else {
            $owner_ip = VisitorHelper::getRealIpAddr();
            $likes = $like_repository->getLikesByIp($owner_ip);
        }
        return $this->render('BlogBundle:Page:index.html.twig',
            ['likes' => $likes,
                'posts' => $posts,
                'page' => $page,
                'size' => $paginator_count,
            ]
        );
    }

    /**
     * @Route("/users", name="users")
     */
    public function usersAction()
    {
        $em = $this->getDoctrine()->getManager();
        /** @var UserRepository $user_repository */
        $user_repository = $em->getRepository('BlogBundle:User');
        $user = $this->getUser();
        $users = $user_repository->findByNot('username', $user->getUsername());
        return $this->render('BlogBundle:Page:users.html.twig', ['users' => $users]);
    }

    /**
     * @Route("/weather", name="weather")
     */
    public function weatherAction()
    {
        $em = $this->getDoctrine()->getManager();
        /** @var WeatherRepository $weather_repository */
        $weather_repository = $em->getRepository('BlogBundle:Weather');
        $weather = $weather_repository->getCurrentForAllCityWeather();
        return $this->render('BlogBundle:Page:about.html.twig', ['weathers' => $weather]);
    }


    /**
     * @Route("/contact", name="contact")
     */
    public function contactAction(Request $request)
    {
        $enquiry = new Contact();

        $form = $this->createForm(ContactForm::class, $enquiry);

        if ($request->isMethod($request::METHOD_POST)) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                //@todo email as service
//                $content = $this->renderView(
//                    'BlogBundle:Page:contactEmail.html.twig',
//                    ['enquiry' => $enquiry]);
//                $this->get('app.mail_service')->sendMessage($enquiry->getEmail(),'Contact request',$content);

                $message = \Swift_Message::newInstance()
                    ->setSubject("Contact request")
                    ->setFrom($enquiry->getEmail())
                    ->setTo($this->getParameter('contact_email'))
                    ->addPart($this->renderView(
                        'BlogBundle:Page:contactEmail.html.twig',
                        ['enquiry' => $enquiry]), 'text/html');

                $this->get('mailer')->send($message);

                $this->get('session')->getFlashBag()->add('blogger-notice', 'Your contact enquiry was successfully sent. Thank you!');

                return $this->redirect($this->generateUrl('contact'));
            }
        }

        return $this->render('BlogBundle:Page:contact.html.twig', ['form' => $form->createView()]);
    }
}