<?php
/**
 * Created by PhpStorm.
 * User: rail
 * Date: 18.11.16
 * Time: 15:41
 */
namespace BlogBundle\Controller;

use BlogBundle\Entity\User;
use BlogBundle\Form\SignupForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\VarDumper\VarDumper;


class SecurityController extends Controller
{

    /**
     * @Route ("/signup" , name= "signup")
     */
    public function signupAction(Request $request)
    {
        // 1) build the form
        $user = new User();
        $form = $this->createForm(SignupForm::class, $user);

        // 2) handle the submit (will only happen on POST)
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid() && $user->saveAvatar()) {
            $this->get('app.security')->register($user);
            return $this->redirect($this->generateUrl('login'));
        }


        return $this->render('BlogBundle:Security:signup.html.twig',
            ['form' => $form->createView()]
        );
    }

    /**
     * @Route("/login", name="login")
     */
    public function loginAction(Request $request)
    {
        /** @var AuthenticationUtils $authenticationUtils */
        $authenticationUtils = $this->get('security.authentication_utils');
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('BlogBundle:Security:login.html.twig', array(
            'last_username' => $lastUsername,
            'error' => $error,
        ));
    }
}