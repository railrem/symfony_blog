<?php
/**
 * Created by PhpStorm.
 * User: rail
 * Date: 30.11.16
 * Time: 21:36
 */

namespace BlogBundle\Controller;


use BlogBundle\Entity\AsyncTask;
use BlogBundle\Entity\Post;
use BlogBundle\Entity\Repository\AsyncTaskRepository;
use BlogBundle\Entity\Repository\PostRepository;
use BlogBundle\Entity\Repository\TransactionRepository;
use BlogBundle\Entity\Transaction;
use BlogBundle\Entity\User;
use BlogBundle\Form\PayForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\VarDumper\VarDumper;

class PaymentController extends Controller
{
    /**
     * @Route("/buy_post", name="buy_post")
     */
    public function actionBuyPost(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var User $user */
        $user = $this->getUser();
        $post_id = $_GET['post_id'];
        $price = 100;
        /** @var  $post_repository PostRepository */
        $post_repository = $em->getRepository('BlogBundle:Post');
        /** @var Post $post */
        $post = $post_repository->find($post_id);
        if (!$post->getIsFree()) {
            if ($user->getBalance() >= $price) {
                $is_yet_buy = false;
                /** @var Post $buy_post */
                foreach ($user->getUserAssignPosts() as $buy_post) {
                    if ($post->getId() == $buy_post->getId()) {
                        $is_yet_buy = true;
                    }
                }
                if (!$is_yet_buy) {
                    $task = new AsyncTask();
                    $task->setContent(json_encode(['source_user_id' => $user->getId(), 'post_id' => $post_id]));
                    /** @var AsyncTaskRepository $task_repository */
                    $task_repository = $em->getRepository('BlogBundle:AsyncTask');
                    $task_repository->saveBuyPostAsyncTask($task);
                } else {
                    $this->get('session')->setFlash(
                        'notice',
                        'Вы уже покупали этот пост!'
                    );
                }
            } else {
                $this->get('session')->setFlash(
                    'notice',
                    'Недостаточно денег пополните кошелек'
                );
            }
        } else {
            $this->get('session')->setFlash(
                'notice',
                'Пост бесплатный его не нужно покупать'
            );
        }
        return $this->redirect($this->generateUrl('post', ['post_id' => $post_id]));
    }

    /**
     * @Route("/pay", name="pay")
     */
    public function actionPay(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var TransactionRepository $transaction_repository */
        $transaction_repository = $em->getRepository('BlogBundle:Transaction');
        $user = $this->getUser();
        $transactions = $transaction_repository->getTransactionByUser($user);
        $transaction = new Transaction();
        $transaction->setIsPayment(true);
        $transaction->setSourceUser($user);
        $transaction->setTargetUser($user);
        $form = $this->createForm(PayForm::class, $transaction);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $task = new AsyncTask();
            $content = json_encode(['user_id' => $user->getId(), 'amount' => $transaction->getAmount()]);
            $task->setContent($content);
            /** @var AsyncTaskRepository $task_repository */
            $task_repository = $em->getRepository('BlogBundle:AsyncTask');
            $task_repository->saveDepositAsyncTask($task);
            $transaction_repository->save($transaction);
            return $this->redirect($this->generateUrl('transactions'));
        }

        return $this->render('BlogBundle:Payment:index.html.twig', ['form' => $form->createView(), 'transactions' => $transactions]);
    }

    /**
     * @Route("/trandactions", name="transactions")
     */
    public function actionTransactions(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var TransactionRepository $transaction_repository */
        $transaction_repository = $em->getRepository('BlogBundle:Transaction');
        $user = $this->getUser();
        $transactions = $transaction_repository->getTransactionByUser($user);

        return $this->render('BlogBundle:Payment:transactions.html.twig', ['transactions' => $transactions]);
    }
}