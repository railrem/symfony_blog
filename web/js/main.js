$(document).ready(function () {
    $(".like").on("click", function () {
        var post_id = $(this).attr('data-post_id');
        var count = $(this).parent().children('.count');
        var like_count = $(this).parent().children('.count').html();
        var button = $(this);
        if (button.hasClass('btn-default')) {
            $.ajax({
                url: '/post/like',
                data: {post_id: post_id},
                success: function (data) {
                    if (data.status == 'success') {
                        like_count = parseInt(like_count) + 1;
                        button.removeClass('btn-default');
                        button.addClass('btn-primary');
                        count.html(like_count);
                    }
                }
            })
        } else {
            $.ajax({
                url: '/post/unlike',
                data: {post_id: post_id},
                success: function (data) {
                    if (data.status == 'success') {
                        like_count = parseInt(like_count) - 1;
                        button.removeClass('btn-primary');
                        button.addClass('btn-default');
                        count.html(like_count);
                    }
                }
            })
        }
    });
});